(set-logic AUFLIA)

(set-option :declare-list-datatype true)
;; equivalent to (declare-datatypes (T) ((List nil (insert (head T) (tail (List T))))))

;; enumeration datatype
(declare-datatypes () ((Color red green blue)))

;; define-sort example using the above
(define-sort ColorList () (List Color))

;; record datatype
(declare-datatypes (S T) ((Pair (mk-pair (first S) (second T)))))

;; some random assertions:
(assert 
 (forall ((l (List Int))) (= (insert (head l) l) (tail l) (as nil (List Int)))))

(declare-const a (Array Int ColorList))

(assert (= (select a 2) (insert red (as nil ColorList))))

(assert (= (mk-pair 1 2) (mk-pair 3 4)))

(assert (let ((c green)) (= (first (mk-pair red 2)) c)))
