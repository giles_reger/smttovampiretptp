;; various tests for the basic functionality of smttotptp
(set-logic AUFLIRA)

(set-option :declare-list-datatype true)
;; This is equivalent to the declaration 
;; (declare-datatypes (T) ((List nil (insert (head T) (tail List)))))
;; See below for examples of other datatype declarations, Z3-style

;; This is how to specify operator precedences f > g > h and f > k > l:
(set-option :prec ("f > g > h" "f > k > l"))
;; It translates into the lines
;;    %$ :prec f > g > h
;;    %$ :prec f > k > l
;; These declarations are honoured by the Beagle prover

(declare-sort Paar 2)
(declare-sort Color 0)
(declare-const k Int)
(declare-const r Real)
(declare-const red Color)
(declare-const green Color)
(declare-const blue Color)
(define-sort MyInt () Int)
(declare-const myk MyInt)
(declare-const a (Array Int Color))
(define-sort Matrix (T) (Array T T))
(define-sort MyMatrix () (Matrix (Paar MyInt Color)))
(declare-fun f (Int MyInt (Array Int Color)) Int) 
(define-fun g ((i Int) (r Real) (j (Array Int Color))) MyInt (+ i k))
(define-fun h ((i MyInt)) Int (let ((k (+ i 1))) (+ k i)))
(define-fun tt ((i Int)) Bool (forall ((j Int)) (<= i j)))
(define-fun succ ((k Color)) Color (ite (= k red) green (ite (= k green) blue red)))
(assert (= (f 2 3 a) (- 5)))
(assert (= red green))
(assert (= (h 5) 8))
(assert (= k 5))
(assert (exists ((db0 Int)) (let ((db1 db0)) (and (not (< db1 db0)) (< db0 8)))))

(assert (let ((?-+<>/=a-zA-Z0-9_!$@~%&*.?^ 200.0)) (= red red)))
(assert (let ((?_+<>/=a-zA-Z0-9_!$@~%&*.?^ 200.0)) (= green green)))

;; some datatype declarations, from Z3 manual
;; records:
(declare-datatypes (T1 T2) ((Pair (mk-pair (first T1) (second T2)))))
(declare-const p1 (Pair Int Int))
(declare-const p2 (Pair Int Int))
(assert (= p1 p2))
(assert (> (second p1) 2))
(assert (not (= (first p1) (first p2))))

;; enumeration types:
(declare-datatypes () ((S A B C)))
(declare-const x S)
(declare-const y S)
(declare-const z S)
(declare-const u S)
(assert (distinct x y z u))

;; lists ("List" with "insert" and "head"/"tail" is predefined)
(declare-datatypes (T) ((Lst nl (cons (hd T) (tl Lst)))))
(declare-const l1 (Lst Int))
(declare-const l2 (Lst Bool))

(assert (not (= (as nl (Lst Int)) (cons 2 l1))))

;; Parametric function declarations are a proprietary extension
(declare-parametric-fun (T) length ((List T)) Int)

(define-fun abc () (List (Pair Int Int)) (as nil (List (Pair Int Int))))

(assert (= (length (insert (mk-pair 1 2) (insert (mk-pair 3 4) abc))) 3))

(assert (= (length (insert 3 (as nil (List Int)))) 4))

;; error:
;; (assert (= (length (insert 3 (as nil (LList Int)))) 4))
(assert (forall ((x Int)) (> x 5)))
(assert (> (- 6) (+ 5 3 4 5)))
(assert (> 2 (let ((x 1)) (+ 2 x))))
(assert (let ((k 5)) (= (ite (> 1 (ite (let ((b true)) (not b)) 0 1)) 2 3) k)))
(assert (let ((t true)) (=> t (and true (= 1 1)))))

(assert (> 2 (let ((x 1) (y 2)) (+ x y))))
(assert (or false (let ((t (> 2 2))) t)))

;; from the SMT-Lib 2 spec:
(declare-fun append ((List Int) (List Int)) (List Int))
(assert 
 (forall ((x (List Int)) (y (List Int)))
	  (= (append x y)
	     (ite (= x (as nil (List Int)))
		  y
		   (let ((h (head x)) (t (tail x)))
		     (insert h (append t y)))))))

;; annotations are ignored
(declare-const xx Int)
(declare-const yy Int)
(declare-const zz Int)
(assert (=> ( ! (> xx yy ) :named $p1 )
       ( ! (= xx zz ) :named $p2 ) ))



