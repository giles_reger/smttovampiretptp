
(set-logic UFLIA)

;; (set-option :expand-ite false)
;; (set-option :expand-let false)
(set-option :expand-bool-quant true)
;; (set-option :elim-tl-bool false)

(assert (let ((x (let ((y 5))
		   (forall ((b Bool)) (ite b (> y 0) (< y 1))))))
		   (= x x)))

(declare-fun f (Bool Int) Int)
(assert
 (forall ((b Bool) (i Int))
	 (= (f b i)
	    (ite b (+ i 1) (- i 1)))))


(assert
 (not
  (= (f (forall ((i Int)) (> i 0)) 5) 4)))


(check-sat)
