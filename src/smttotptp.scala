package smttotptp

import util._

object smttotptp {

  val version = "0.9.4"
  val date = "13/10/2015"

  def usage() {
    System.err.println("Usage: smttotptp [OPTION ...] INFILE [OUTFILE]")
    System.err.println()
    System.err.println("Transform the SMT-Lib file INFILE into TPTP TFF format in OUTFILE.")
    System.err.println("If OUTFILE is not given its name is derived from INFILE.")
    System.err.println("OPTION:")
    System.err.println(" --help:      this message")
    System.err.println(" --version:   show the version number and exit")
    System.err.println(" --lists:     declare the list datatype and emit corresponding axioms")
    System.err.println(" --arrays:    declare store/select/const array operators and emit corresponding axioms")
    System.err.println(" --quiet:     suppress all diagnostic output to stdout")
    System.err.println(" --pp:        pretty-print the TPTP formula using indentation. (May lead to significantly larger file.)")
    System.err.println(" --stdout:    print result to stdout instead of OUTFILE. Implies --quiet")
    sys.exit(1)
  }

  // See README.md for currently supported options

  var diagnosticOut: Option[StringListBuffer] = None // Used in message, set by --stdout flag

  var ppFlag = false

  def message(s: String) {
    diagnosticOut match {
      case None => System.err.println(s)
      case Some(d) => d.println("%% " + s)
    }
  }

  def warning(msg: String) {
    message("Warning: " + msg)
  }

  def main(args: Array[String]) {

    if (args.isEmpty) {
      System.err.println("smttotptp: no arguments given. Try 'smttotptp --help'")
      sys.exit(1)
    }

    var params = args.toList
    var quietFlag = false
    var stdoutFlag = false
    var listsFlag = false
    var arraysFlag = false
    // var keepBoolFlag = false

    var done = false

    // where the output really goes
    val out = new StringListBuffer()
    out.println("%% SZS status Success")
    out.println("%% SZS output start ListOfTFF")

    while ((!done) && (!params.isEmpty))
      params match {
        case "--quiet" :: _ => {
          quietFlag = true
          params = params.tail
        }
        case "--arrays" :: _ => {
          arraysFlag = true
          params = params.tail
        }
        case "--lists" :: _ => {
          listsFlag = true
          params = params.tail
        }
        case "--pp" :: _ => {
          ppFlag = true
          params = params.tail
        }
        // case "--keepBool" :: _ => {
        //   keepBoolFlag = true
        //   params = params.tail
        // }
        case "--help" :: _ => usage()
        case "--version" :: _ => {
          println("smttotptp version %s (%s)".format(version, date))
          sys.exit(1)
        }
        case "--stdout" :: _ =>
          stdoutFlag = true
          quietFlag = true
          diagnosticOut = Some(out)
          params = params.tail
        case _ => done = true
      }
    if (params.isEmpty) {
      System.err.println("smttotptp: no input file given. Try 'smttotptp --help'")
      sys.exit(1)
    }

    val inFileName = params.head
    params = params.tail

    val outFileName =
      if (!params.isEmpty) params.head
      else ({
        val h1 = inFileName.stripSuffix(".smt")
        if (h1 != inFileName) h1
        else {
          val h2 = inFileName.stripSuffix(".smt2")
          if (h2 != inFileName) h2 else inFileName
        }
      }) + ".p"

    var outStream:java.io.PrintStream = null

    def errorMsg(s: String) {
      if (!stdoutFlag) {
        System.err.println("Error: " + s)
      }
      if (outStream != null) {
        outStream.println("%% SZS status Error")
        outStream.println("%% " + s)
      }
    }

    try {
      val cmds = SExprParser.parseSMTLibFile(inFileName)

      // val out = if (stdoutFlag) System.out else new java.io.PrintStream(new java.io.FileOutputStream(outFileName))
      val ast = new AST

      try {
        // If array/list command line options are given we need to declare them before parsing.
        // Notice corresponding option settings in the input file are possibly overridden
        if (listsFlag) ast.addListDatatype()
        if (arraysFlag) ast.addArrayDeclarations()

        // println("Parsing into abstract syntax")
        ast.parseCommands(cmds)
        import ast._

        logic match {
          case None => warning("no set-logic command, assuming LIRA")
          case Some(l) => out.println("%% (logic) " + l)
        }

        infos foreach { info =>
          {
            out.println("%% (info) " + info.key + " " + (info.value match {
              case None => ""
              case Some(v) => v.toString.replaceAll("\n", "\n%% ")
            }))
          }
        }

        options foreach { 
          _ match {
            case Attribute(Keyword(":prec"), value) => {
              // :prec needs special treatment
              value match {
                case None => ()
                case Some(StringSExpr(s)) => 
                  out.println("%$ :prec " + s)
                case Some(SExprList(ss)) => 
                  ss foreach { 
                    _ match {
                      case StringSExpr(s) => out.println("%$ :prec " + s)
                      case _ => warning("malformed :prec option: " + value)
                    }
                  }
                case _ => warning("malformed :prec option: " + value)
              }
            }
            case option => {
              out.println("%$ " + option.key + " " + (option.value match {
                case None => ""
                case Some(v) => v.toString.replaceAll("\n", "\n%% ")
              }))
            }
          }
        }

        // println("Options given:")
        // ast.options foreach { println(_) }

        if (!quietFlag) ast.show()

        // println("Expanding defined functions")
        ast.expandDefinedFunctions()

        // Can add axioms only now, after function definitions have been expanded
        // if ((getOptionStringValue(":declare-arrays") == Some("true")) || arraysFlag)
        ast.addArrayAxioms()

        ast.addDatatypeAxioms()

        // In Vampire we want to expand Let ourselves
        //if (!(getOptionStringValue(":expand-let") == Some("false")))
        //  ast.expandLet()

        // In Vampire we want to texpand Ite ourselves
        //if (!(getOptionStringValue(":expand-ite") == Some("false")))
        //  ast.expandIte()

        // In Vampire $o is a first class sort
        //if (getOptionStringValue(":expand-bool-quant") == Some("true"))
        //  ast.expandBooleanQuantification()
        //
        //if (!(getOptionStringValue(":elim-tl-bool") == Some("false")))
        //  ast.elimBool()

        ast.simplifyAssertions()

        val pr = new TPTPPrinter(out)
        try {
          outStream =
            if (stdoutFlag) System.out else new java.io.PrintStream(new java.io.FileOutputStream(outFileName))

          pr.print(ast)
          out.println("%% SZS output end ListOfTFF")
          out.println("%% " + "File generated by smttotptp version %s (%s) from %s".format(version, date, inFileName))
          out.writeOut(outStream)
          if (!quietFlag) println("Output written to " + outFileName)
        } catch {
          case pr.Error(s) => {
            errorMsg("TPTP output: " + s)
            sys.exit(1)
          }
        }
      } catch {
        case ast.Error(s) => {
          errorMsg("AST error: " + s)
          sys.exit(1)
        }
      }
    } catch {
      case x: java.io.FileNotFoundException ⇒ {
        errorMsg(x.toString )
        sys.exit(1)
      }
      case SExprParser.Error(s) => {
        errorMsg("SExpr parser error: " + s)
        sys.exit(1)
      }
    }
  }
}
