package smttotptp

/**
 * An S-Expression parser for SMT-Lib 2
 */

import scala.util.matching.Regex
import scala.util.parsing.combinator._
import java.io.FileReader

import util._

class SExprParser extends JavaTokenParsers with PackratParsers {

  /**
   * The grammar rules
   */
  lazy val SMTLibInput: PackratParser[List[SExpr]] = 
    rep(pSExpr)

  lazy val pSExpr: PackratParser[SExpr] = 
    pComment | pSpecConstant | pSymbol | pKeyword | pSExprList

  lazy val pSExprList: PackratParser[SExpr] = 
    "(" ~> rep(pSExpr) <~ ")" ^^ {
      case sexprs => SExprList(sexprs)
    } 

  lazy val pSpecConstant = 
    pDecimal | pNumeral | pString

  lazy val pNumeral = 
    ("""[1-9][0-9]*""".r <~ guard(not(".")) ^^ { case s => Numeral(s.toInt) }) | 
    ("0" <~ guard(not(".")) ^^ { case _ => Numeral(0) })

  lazy val pDecimal = 
    """[0-9]+""".r ~ "." ~ """[0-9]+""".r ^^ {
      case n ~ _ ~ d => Decimal(Rat((n + d).toInt, ("1" + "0"*d.length).toInt))
    }

  lazy val pString =
   """"[^"]*"""".r ^^ { case s => StringSExpr(s.drop(1).dropRight(1)) }

/*  lazy val pSymbol = 
     ( """[-+<>/=a-zA-Z_!$@][-+<>/=a-zA-Z0-9_!$@]*""".r ^^ { case s => Symbol(s) } ) |
     ( """\|[^\|]*\|""".r ^^ { case s => Symbol(s.drop(1).dropRight(1)) } )  // drop the bars
 */
// ~!@$%^&*_-+=<>.?/

  lazy val baredSymbol = 
     ( """\|[^\|]+\|""".r ^^ { case s => Symbol(s.drop(1).dropRight(1)) } ) // drop the bars
  lazy val pSymbol = 
     ( """[-+<>/=a-zA-Z_!$@~%&*.?^][-+<>/=a-zA-Z0-9_!$@~%&*.?^]*""".r ^^ { case s => Symbol(s) } ) |
     baredSymbol

  lazy val pKeyword = 
      """:[-+<>/=a-zA-Z0-9_!$@~%&*.?^]*""".r ^^ { case s => Keyword(s) }

  lazy val pComment =
    """;.*""".r ^^ (x ⇒ FunApp1("__comment__", StringSExpr(x)))

  lazy val pAttributeValue = 
    pSpecConstant | pSymbol | pSExprList

  lazy val pAttribute = pKeyword ~ opt(pAttributeValue)

}

object SExprParser {

  case class Error(s: String) extends Exception

  val parser = new SExprParser()

  def isComment(sexpr: SExpr) = sexpr match { case FunApp1("__comment__", _) => true; case _ => false }

  // todo: code duplication below for error checking - get rid of that

  def parseSExpr(s: String): SExpr = {
    // parser.parseAll[SExpr](parser.pSExpr, s).get
    import parser._
    try {
      parseAll[SExpr](pSExpr, s) match {
        case Success(x, _) => x
        case NoSuccess(err, next) => {
          throw this.Error("Failed to parse input file: (line " + next.pos.line +
              ", column " + next.pos.column + "):\n" + err + "\n" +
              next.pos.longString)
        }
      }
    } catch {
      case e : IllegalArgumentException ⇒ // caused by dangling references
        throw this.Error("Failed to parse input file")
    }
  }

  def parseSExprs(s: String): List[SExpr] = {
    // parser.parseAll[List[SExpr]](parser.SMTLibInput, s).get  filterNot { isComment(_) }
    import parser._
    try {
      parseAll[List[SExpr]](SMTLibInput, s)  match {
        case Success(x, _) => x filterNot { isComment(_) }
        case NoSuccess(err, next) => {
          throw this.Error("Failed to parse input file: (line " + next.pos.line +
              ", column " + next.pos.column + "):\n" + err + "\n" +
              next.pos.longString)
        }
      }
    } catch {
      case e : IllegalArgumentException ⇒ // caused by dangling references
        throw this.Error("Failed to parse input file")
    }

  }

  def parseSMTLibFile(fileName: String) = {
    // parseAll[List[SExpr]](SMTLibInput, s).get  filterNot { isComment(_) }
    val reader = new FileReader(fileName)
    import parser._
    try {
      parseAll[List[SExpr]](SMTLibInput, reader) match {
        case Success(x, _) => x filterNot { isComment(_) }
        case NoSuccess(err, next) => {
          throw this.Error("Failed to parse input file: (line " + next.pos.line +
            ", column " + next.pos.column + "):\n" + err + "\n" +
            next.pos.longString)
        }
      }
    } catch {
      case e : IllegalArgumentException ⇒ // caused by dangling references
        throw this.Error("Failed to parse input file")
    }
  }

}
