package smttotptp

object util {

  class Counter {
    var i = 0
    def next() = {
      i += 1
      i
    }
  }

  class MyList[T](l: List[T]) {

    def toMyString(toStringFn: T ⇒ String, ifEmpty: String, lsep: String, isep: String, rsep: String): String =
      if (l.isEmpty)
        ifEmpty
      else
        lsep + l.map(toStringFn(_)).reduceLeft(_ + isep + _) + rsep

    // Convenience Functions
    def toMyString(ifEmpty: String, lsep: String, isep: String, rsep: String): String =
      toMyString(x ⇒ x.toString, ifEmpty, lsep, isep, rsep)

    def toMyString(lsep: String, isep: String, rsep: String): String =
      toMyString(lsep + rsep, lsep, isep, rsep)

  }

  implicit def toMyList[T](l: List[T]) = new MyList(l)


  import scala.collection.mutable.ListBuffer // where the output goes

  class StringListBuffer() {

    val lines = new ListBuffer[String]()
    def println(s: String) {
      lines.append(s)
    }
    def println() {
      lines.append("")
    }

    def writeOut(out: java.io.PrintStream) {
      for (l <- lines) out.println(l)
      out.close()
    }
  }
}
