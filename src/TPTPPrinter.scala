package smttotptp

import util._
import scala.collection.immutable.ListMap

class TPTPPrinter(out: StringListBuffer) {

  case class Error(s: String) extends Exception

  // not needed - have unquoted already in the SExpr parser
  // def unQuoteSMT(s: String) = if (s.head == '|' && s.last == '|') s.drop(1).dropRight(1) else s

  def isLowerAlpha(c: Char) = 'a' <= c && c <= 'z'
  def isUpperAlpha(c: Char) = 'A' <= c && c <= 'Z'
  def isNumeric(c: Char) = '0' <= c && c <= '9'
  def isAlphaNumeric(c: Char) = isLowerAlpha(c) || isUpperAlpha(c) || isNumeric(c) || c == '_'
  def isLowerWord(s: String) = isLowerAlpha(s.head) && (s.tail forall { isAlphaNumeric(_) })

  def toAlphaNumericIfPossible(s: String) =
    // This may collapse different identifiers, but it is probably safe ...
    s.replaceAll("-", "_")

  def toAtomicWord(s: String) = {
    assume(!s.isEmpty)
    val hs = toAlphaNumericIfPossible(s)
    if (isLowerWord(hs))
      hs
    else
      // give up and quote 
      "'" + hs + "'"
  }

  def isTPTPVar(s: String) =
    isUpperAlpha(s.head) && (s.tail forall { isAlphaNumeric(_) })

  val varCtr = new Counter()
  def newVar(s: String) = "%s__%d__".format(s, varCtr.next())

  def canonical(s: String) = {
    // Replace non-alphanumeric characters by '_'
    val h1 = s map {
      _ match {
        case '~' | '!' | '@' | '$' | '%' | '^' | '&' | '*' | '-' | '+' | '=' | '<' | '>' | '.' | '?' | '/' => '_'
        case c => c
      }
    }
    // See if h1 starts with an uppercase letter or can be converted into such
    if (isUpperAlpha(h1.head))
      h1
    else if (isLowerAlpha(h1.head))
      h1.head.toUpper + h1.tail
    else 
      // Something different - turn into a variable by prefixing with A_
      "A_" + h1
  }

  var specialVarMap = Map.empty[String, String]
  var reverseMap = Map.empty[String, String]

  def toTPTPVar(s: String) = {
    if (isTPTPVar(s))
      s 
    else {
      val sc = canonical(s)
      specialVarMap.get(sc) match {
        case Some(h) => h // readily use this
        case None => {
          // We see if we can use sc. This is possible if no  previous variable
          // different to s has the same canonical name
          reverseMap.get(sc) match {
            case None => {
              // Can use sc, but remember the pair sc -> c
              reverseMap += ((sc -> s))
              sc
            }
            case Some(t) if s == t => 
              // no conflict
              sc
            case Some(_) => {
              // conflicting previous string
              val res = newVar(sc)
              specialVarMap += ((s -> res))
              res
            }
          }
        }
      }
    }
  }


  def print(ast: AST) {
    import ast._

    // Let-expressions in SMT-LIB bind variables, whereas in Vampire TPTP they bind function symbols.
    // That means that variables, bound in let-expressions should be translated as constants.
    // For that, we will maintain the set of variables, bound in a let-expression and will print them
    // in lowercase (as constants). Variables, bound in a quantifier we will print in uppercase.
    def varToTPTP(v: Var, letVars: Set[Var]) = if (letVars contains v) v.baseName else toTPTPVar(v.toString)

/*    def varToTPTP(v: Var) = {
      val name = toAlphaNumericIfPossible(v.toString)
      if (name exists { !isAlphaNumeric(_) })
        throw this.Error("cannot convert into a TPTP variable: " + v)
      if (isUpperAlpha(name.head))
        name
      else if (isLowerAlpha(name.head))
        name.head.toUpper + name.tail
      else
        throw this.Error("cannot convert into a TPTP variable: " + v)
    }
 */ 
    // The concrete sorts used in all terms
    var sorts = Set.empty[CSort]

    def varWithSortToTPTP(vs: (Var, CSort), letVars: Set[Var]) = varToTPTP(vs._1, letVars) + ":" + sortToTPTP(vs._2)

    def sortToTPTP(sort: CSort, toAtomicWordFlag: Boolean = true): String = {
      val CSort(SortSym(name), args) = sort
      name match {
        case "Bool" if toAtomicWordFlag => "$o"
        case "Int" if toAtomicWordFlag => "$int"
        case "Real" if toAtomicWordFlag => "$real"
        // TODO here the recursive call of sortToTPTP should be true for built-in sorts and false for user-defined sorts
        case "Array" if toAtomicWordFlag => "$array" + (args map { sortToTPTP(_,true) }).toMyString("","(",",",")")
        case _ => {
          val s = name + (args map { sortToTPTP(_, false) }).toMyString("", "[", ",", "]")
          if (toAtomicWordFlag) toAtomicWord(s) else s
        }
      }
    }

    val tffPredefSorts = Set(IntSort, RealSort, BoolSort)

    def funToTPTP(fun: FunSym, argsSorts: List[CSort], resSort: CSort) = {
      val FunSym(name) = fun
      val m = Map(
        "true" -> "$true",
        "false" -> "$false",
        "<" -> "$less",
        ">" -> "$greater",
        "<=" -> "$lesseq",
        ">=" -> "$greatereq",
        "+" -> "$sum",
        "-" -> "$difference",
        "*" -> "$product",
        "/" -> "$quotient",
        "select" -> "$select",
        "store" -> "$store"
      )

      // unary minus is a special case, as it's overloaded with subtraction
      if ((name == "-") && (argsSorts.length == 1))
        "$uminus"
      else m.get(name) match {
        case Some(s) => s
        case None => if ((declaredCFuns contains fun) || (definedFuns contains fun))
          // non-parametric case, don't need sort annotations to make the function name unique
          toAtomicWord(name)
        else
          toAtomicWord(
            name + ":" + (argsSorts map { sortToTPTP(_, false) }).toMyString("", "(", "*", ")") +
              (if (argsSorts.isEmpty) "" else ">") + sortToTPTP(resSort, false))
      }
    }

    def funRankToTPTP(funRank: (FunSym, List[CSort], CSort)) = {
      val (fun, argsSorts, resSort) = funRank
      funToTPTP(fun, argsSorts, resSort) + ": " +
        (if (argsSorts.isEmpty) ""
        else
          ((if (argsSorts.tail.isEmpty)
            sortToTPTP(argsSorts.head)
          else
            (argsSorts map { sortToTPTP(_) }).toMyString("", "(", " * ", ")")) + " > ")) + sortToTPTP(resSort)
    }

    var funRanks = Set.empty[(FunSym, List[CSort], CSort)]

    // FunSym("and") and FunSym("or") are excluded from leftAssocOps because we treat it specially,
    // see toTPTP
    val leftAssocOps = Set(FunSym("xor"), FunSym("+"), FunSym("-"), FunSym("*"), FunSym("/"), FunSym("div"))
    val rightAssocOps = Set(FunSym("=>"), FunSym("implies"))
    val chainableOps = Set(FunSym("="), FunSym("<"), FunSym(">"), FunSym("<="), FunSym(">="))
    val pairwiseOps = Set(FunSym("distinct"))

    def expandLeftAssoc(fun: FunSym, args: List[Term]) = args.reduceRight((s, t) => App(fun, List(s, t)))
    def expandRightAssoc(fun: FunSym, args: List[Term]) = args.reduceLeft((s, t) => App(fun, List(s, t)))
    def expandChainable(op: FunSym, args: List[Term]) =
      App(FunSym("and"),
        (for (
          ts <- args.tails;
          if (!ts.isEmpty) && (!ts.tail.isEmpty)
        ) yield App(op, List(ts.head, ts.tail.head))).toList)
    def expandPairwise(op: FunSym, args: List[Term]) =
      App(FunSym("and"),
        (for (
          ss <- args.tails;
          if (!ss.isEmpty);
          ts <- ss.tail.tails;
          if (!ts.isEmpty)
        ) yield App(op, List(ss.head, ts.head))).toList)

    def nlspaces(n: Int) =
      if (smttotptp.ppFlag)
        "\n" + ("\t" * (n / 8)) +  (" " * (n % 8))
      else "\n"

    // pretty print a list of terms with parenthesis and a separator
    def toTPTPList(ts: List[Term], varSort: ListMap[Var, CSort], letVars: Set[Var], left: String, sep: String, right: String, indent: Int): String = {
      left + " " +
        (for (tss <- ts.tails; if !tss.isEmpty) yield {
          toTPTP(tss.head, varSort, letVars, indent + left.length + 1) +
            // More formulas?
            (if (!tss.tail.isEmpty) {
              nlspaces(indent - (sep.length - 1)) +
                sep + " "
            } else
              "")
        }).mkString + ")"
    }

    def toTPTP(t: Term, varSort: ListMap[Var, CSort], letVars: Set[Var], indent: Int, topLevel: Boolean = false): String = {
      // topLevel tells us if toTPTP works on the top level of an assertion
      if (!t.attributes.isEmpty)
        smttotptp.warning("ignoring attributes" + t.attributes.toMyString(" ", " ", " ") + "of term " + t)

      val resSort = t.xsort(varSort)
      sorts += resSort

      t match {
        case Const(const) =>
          const match {
            case Numeral(n) => n.toString
              // Bug fix: output decimals, not rationals
            // case Decimal(Rat(num, denom)) => num + "/" + denom
            case Decimal(r) => r.toDouble.toString
            case StringSExpr(s) => s
          }
        case v: Var => varToTPTP(v, letVars)

          // Special case: distinct at top-level
          // todo: check that arguments are constants
        case App(FunSym("distinct"), args) if
          topLevel && (getOptionStringValue(":expand-distinct") == Some("false")) =>
          "$distinct" + (args map { toTPTP(_, varSort, letVars, indent+1) }).mkString("(", ", ", ")")

        // Applications of associatove, chainable and pairwise operators:
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (leftAssocOps contains fun) =>
          toTPTP(expandLeftAssoc(fun, args), varSort, letVars, indent)
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (rightAssocOps contains fun) =>
          toTPTP(expandRightAssoc(fun, args), varSort, letVars, indent)
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (chainableOps contains fun) =>
          toTPTP(expandChainable(fun, args), varSort, letVars, indent)
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (pairwiseOps contains fun) =>
          toTPTP(expandPairwise(fun, args), varSort, letVars, indent)

        // special cases
        case App(FunSym("="), List(s, t)) if (s.xsort(varSort) == BoolSort) =>
          // could be longish, print in two lines
          "( " + toTPTP(s, varSort, letVars, indent + 2) +
            nlspaces(indent - 2) + "<=> " + toTPTP(t, varSort, letVars, indent + 2) + ")"

        case App(FunSym("="), List(s, t)) =>
          "(" + toTPTP(s, varSort, letVars, indent + 1) + " = " + toTPTP(t, varSort, letVars, indent + 1) + ")"

        case App(FunSym("not"), List(App(FunSym("="), List(s, t)))) if s.xsort(varSort) != BoolSort =>
          "(" + toTPTP(s, varSort, letVars, indent + 1) + " != " + toTPTP(t, varSort, letVars, indent + 1) + ")"

        case App(FunSym("distinct"), List(s, t)) =>
          "(" + toTPTP(s, varSort, letVars, indent + 1) + " != " + toTPTP(t, varSort, letVars, indent + 1) + ")"

        case App(FunSym("and"), args) => args.length match {
          case 0 => "$true"
          case 1 => toTPTP(args(0), varSort, letVars, indent)
          case _ => toTPTPList(args, varSort, letVars, "(", "&", ")", indent)
        }
        case App(FunSym("or"), args) => args.length match {
          case 0 => "$false"
          case 1 => toTPTP(args(0), varSort, letVars, indent + 1)
          case _ => toTPTPList(args, varSort, letVars, "(", "|", ")", indent)
        }
        case App(FunSym("not"), List(arg)) => "(~ " + toTPTP(arg, varSort, letVars, indent + 3) + ")"

        case App(FunSym(op), List(s, t)) if op == "implies" || op == "=>" =>
          "( " + toTPTP(s, varSort, letVars, indent + 2) + nlspaces(indent - 1) + "=> " + toTPTP(t, varSort, letVars, indent + 2) + ")"

        case App(FunSym("ite"), List(c, t, e)) =>
            "$ite(" +
            toTPTP(c, varSort, letVars, indent + 7) + "," +
            nlspaces(indent + 2) + toTPTP(t, varSort, letVars, indent + 2) + "," +
            nlspaces(indent + 2) + toTPTP(e, varSort, letVars, indent + 2) + ")"

        // "ordinary" function applications
        case App(fun @ FunSym(name), args) => {
          val argsSorts = args map { _.xsort(varSort) }
          funRanks += ((fun, argsSorts, resSort))
          funToTPTP(fun, argsSorts, resSort) + (args map { toTPTP(_, varSort, letVars, indent + 1) }).toMyString("", "(", ", ", ")")
        }

        case App(As(fun @ FunSym(name), asSort), args) => {
          sorts += asSort
          val argsSorts = args map { _.xsort(varSort) }
          funRanks += ((fun, argsSorts, asSort))
          funToTPTP(fun, argsSorts, asSort) + (args map { toTPTP(_, varSort, letVars, indent + 1) }).toMyString("", "(", ", ", ")")
        }

        case Exists(varsWithSorts, body) => {
          "( ? " + (varsWithSorts map { varWithSortToTPTP(_, letVars) }).toMyString("[", ", ", "]") + " :" +
            nlspaces(indent + 4) + toTPTP(body, varSort ++ varsWithSorts, letVars, indent + 4) + ")"
        }

        case Forall(varsWithSorts, body) => {
          "( ! " + (varsWithSorts map { varWithSortToTPTP(_, letVars) }).toMyString("[", ", ", "]") + " :" +
            nlspaces(indent + 4) + toTPTP(body, varSort ++ varsWithSorts, letVars, indent + 4) + ")"
        }

        case Let(bindings, body) => {
          val bodyVarSort = varSort ++ (bindings map { case Equal(v, t) => (v, t.xsort(varSort)) })
          val vars = bindings map { _.v } toSet
          val strBindings = bindings map { case Equal(v, t) => varToTPTP(v, letVars + v) + " := " + toTPTP(t, varSort, letVars, indent + 4) } mkString "; "
          "$let(" + strBindings + "," +
          nlspaces(indent + 2)  + toTPTP(body, bodyVarSort, letVars ++ vars, indent + 2) + ")"
        }

        case _ => throw this.Error("cannot convert formula: " + t)
      }
    }

    // body of print

    // Don't need to write declarations for predefined operators
    val tffPredefFuns = Set(FunSym("true"), FunSym("false"), FunSym("<"), FunSym(">"), FunSym("<="), FunSym(">="), 
                            FunSym("+"), FunSym("-"), FunSym("*"), FunSym("/"), FunSym("select"), FunSym("store"))

    val tptpAssertions = assertions map { a =>
      assert(a._1.xsort() == BoolSort)
      // as a side effect, this will populate sorts and funRanks:
      toTPTP(a._1, ListMap.empty, Set.empty, 2, topLevel = true)
    }

    out.println("%% Types:")
    for (
      sort <- sorts;
      if (!(tffPredefSorts contains sort))
    ){
     // Stop us from printing Array sorts
      val CSort(SortSym(name), args) = sort
      name match {
       case "Array" => 
       case _  => out.println("tff(" + toAtomicWord(sort.sortSym.name) + ", type, " + sortToTPTP(sort) + ": $tType).")
      }
    }
    out.println()

    out.println("%% Declarations:")
    for (
      funRank <- funRanks;
      if (!(tffPredefFuns contains funRank._1))
    ) out.println("tff(" + toAtomicWord(funRank._1.name) + ", type, " + funRankToTPTP(funRank) + ").")
    out.println()

    out.println("%% Assertions:")
    for ((a, f) <- assertions zip tptpAssertions) {
      out.println("%% " + a._2)
      out.println("tff(formula, axiom," + nlspaces(2) + f + ").")
      out.println()
    }
  }

}
