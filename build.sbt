import AssemblyKeys._

name := "smttotptp"

version := "0.9.4"

scalaVersion := "2.11.7"

scalaSource in Compile <<= baseDirectory(_ / "src/")

// unmanagedBase in (Compile, run) <<= baseDirectory(_ / "lib/")

mainClass in (Compile, run) := Some("smttotptp.main")

retrieveManaged := true

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.1"

scalacOptions += "-g:vars"

// scalacOptions += "-unchecked"

// scalacOptions += "-optimise"

// Use Scala from a directory on the filesystem instead of retrieving from a repository
// scalaHome := Some(file("/usr/local/scala")),

scalacOptions ++= List("-feature", "-deprecation")

scalacOptions ++= List("-language:implicitConversions", "-language:postfixOps")

assemblySettings

mainClass in assembly := Some("smttotptp.smttotptp")

jarName in assembly := "smttotptp-" + version.value + ".jar"

test in assembly := {}

